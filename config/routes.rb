Rails.application.routes.draw do
  namespace :admin do
    resources :starters
    resources :sidedishes
    resources :currydishes
    resources :currynames
    resources :chefspecials
    resources :biriyanis
    resources :ricedishes
    resources :fishdishes
    resources :sundries
    resources :small_dishes

    root to: "starters#index"
  end
  resources :pages, only:[:create, :new,:show, :home ]
  root "pages#home"
  
  # get '/:slug' =>'pages#show', as: :curry
  
  
  match "*path", to: "errors#not_found", via: :all
  match "*path", to: "errors#internal_server_error", via: :all
end
