# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190423095822) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "biriyanis", force: :cascade do |t|
    t.string "title"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "chefspecials", force: :cascade do |t|
    t.string "title"
    t.string "desc"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "currydishes", force: :cascade do |t|
    t.string "title"
    t.text "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.string "slug"
    t.index ["slug"], name: "index_currydishes_on_slug", unique: true
  end

  create_table "currynames", force: :cascade do |t|
    t.string "name"
    t.decimal "amt"
    t.bigint "currydish_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["currydish_id"], name: "index_currynames_on_currydish_id"
  end

  create_table "fishdishes", force: :cascade do |t|
    t.string "title"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "ricedishes", force: :cascade do |t|
    t.string "title"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "sidedishes", force: :cascade do |t|
    t.string "title"
    t.text "desc"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "small_dishes", force: :cascade do |t|
    t.string "dish_type"
    t.string "dish_name"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "starters", force: :cascade do |t|
    t.string "title"
    t.text "desc"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "sundries", force: :cascade do |t|
    t.string "title"
    t.decimal "amt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "currynames", "currydishes"
end
