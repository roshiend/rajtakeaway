class CreateRicedishes < ActiveRecord::Migration[5.1]
  def change
    create_table :ricedishes do |t|
      t.string :title
      t.decimal :amt
      t.timestamps
    end
  end
end
