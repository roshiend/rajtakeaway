class AddAttachmentImageToSidedishes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :sidedishes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :sidedishes, :image
  end
end
