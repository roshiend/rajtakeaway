class AddAttachmentImageToChefspecials < ActiveRecord::Migration[5.1]
  def self.up
    change_table :chefspecials do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :chefspecials, :image
  end
end
