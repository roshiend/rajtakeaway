class CreateStarters < ActiveRecord::Migration[5.1]
  def change
    create_table :starters do |t|
      t.string :title
      t.text :desc
      t.decimal :amt
      
      t.timestamps
    end
  end
end
