class CreateChefspecials < ActiveRecord::Migration[5.1]
  def change
    create_table :chefspecials do |t|
      t.string :title
      t.string :desc
      t.decimal :amt
      t.timestamps
    end
  end
end
