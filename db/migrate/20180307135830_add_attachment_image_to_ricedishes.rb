class AddAttachmentImageToRicedishes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :ricedishes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :ricedishes, :image
  end
end
