class AddAttachmentImageToBiriyanis < ActiveRecord::Migration[5.1]
  def self.up
    change_table :biriyanis do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :biriyanis, :image
  end
end
