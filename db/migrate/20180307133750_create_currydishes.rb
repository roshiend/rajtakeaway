class CreateCurrydishes < ActiveRecord::Migration[5.1]
  def change
    create_table :currydishes do |t|
      t.string :title
      t.text :desc
      t.timestamps
    end
  end
end
