class AddAttachmentImageToStarters < ActiveRecord::Migration[5.1]
  def self.up
    change_table :starters do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :starters, :image
  end
end
