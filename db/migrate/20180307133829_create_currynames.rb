class CreateCurrynames < ActiveRecord::Migration[5.1]
  def change
    create_table :currynames do |t|
      t.string :name
      t.decimal :amt
      t.belongs_to :currydish, index: true, foreign_key: true
      t.timestamps
    end
  end
end
