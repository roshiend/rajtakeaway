class CreateFishdishes < ActiveRecord::Migration[5.1]
  def change
    create_table :fishdishes do |t|
      t.string :title
      t.decimal :amt
      t.timestamps
    end
  end
end
