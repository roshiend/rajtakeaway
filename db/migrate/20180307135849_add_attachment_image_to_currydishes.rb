class AddAttachmentImageToCurrydishes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :currydishes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :currydishes, :image
  end
end
