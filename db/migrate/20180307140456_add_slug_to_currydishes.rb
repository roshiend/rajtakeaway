class AddSlugToCurrydishes < ActiveRecord::Migration[5.1]
  def change
    add_column :currydishes, :slug, :string
    add_index :currydishes, :slug, unique: true
  end
end
