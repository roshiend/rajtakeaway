class CreateBiriyanis < ActiveRecord::Migration[5.1]
  def change
    create_table :biriyanis do |t|
      t.string :title
      t.decimal :amt
      t.timestamps
    end
  end
end
