class CreateSmallDishes < ActiveRecord::Migration[5.1]
  def change
    create_table :small_dishes do |t|
      t.string :dish_type # meat/veg
      t.string :dish_name # kurma/ mossola/ rogon..
      t.decimal :amt
      t.timestamps
    end
  end
end
