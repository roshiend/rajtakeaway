class PagesController < ApplicationController
  def home
    @chefspecials = Chefspecial.all.order("ID")
    @biriyanis = Biriyani.all.order("ID")
    @currydishes =Currydish.all.order("ID")
    @currynames =Curryname.all.order("ID")
    @sundries = Sundry.all.order("ID")
    @starters =Starter.all.order("ID")
    @sidedishes = Sidedish.all.order("ID")
    @ricedishes = Ricedish.all.order("ID")
    @sundries = Sundry.all.order("ID")
    @fishdishes= Fishdish.all.order("ID")
    @smalldishes= SmallDish.all.order("ID")
    @page = Page.new  
    
    
    
                        
  end
  
  def create
   @page = Page.new(contact_params)
    @page.request = request
    respond_to do |format|
      if @page.deliver
        # re-initialize Home object for cleared form
       # @page = Page.new
        format.html { redirect_to root_path puts " sent via  html"}
        format.js   { redirect_to root_path}
      else
        format.html { redirect_to root_path puts " reject via  html"}
        format.js   {redirect_to root_path }
        
        
      end
    end
  end 
  
  def show
    # @currydish = Currydish.find_by_slug(params[:slug])
    @currydish = Currydish.friendly.find(params[:id])
     
      
      respond_to do |format|
        format.html 
        format.js 
        
      end
     
    
  
      if @currydish.nil?
        redirect_to root_path # redirect to home page
        
      end
  end
  
  private
    def contact_params
      
      params.require(:page).permit(:name, :email, :message, :nickname)
    end 
end
