module Admin
  class CurrydishesController < Admin::ApplicationController
    def find_resource(param)
       Currydish.friendly.find_by!(slug: param)
    end
  end
end
