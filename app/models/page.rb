class Page< MailForm::Base
     attributes :name,  :validate => true
     attributes :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
     attributes :message,  :validate => true
     attributes :nickname,  :captcha => true # prevent spam using captcha
    
   # # define header for email
    def headers
     {
       :subject => "Contact Form",
       :to => "roshiend@yahoo.com",
       :from => %("#{name}" <#{email}>)
     }
    end  
end