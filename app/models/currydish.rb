class Currydish < ApplicationRecord
    has_attached_file :image, styles: { medium: "300x300", thumb: "100x100#" }
    validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
    
    extend FriendlyId
    friendly_id :title, use: :slugged
    
    has_many :currynames
end
